# XPOS_Porto

![Screenshot](./catalog.png)

XPOS merupakan project yang dibuat saat saya menjalani bootcamp .Net di Xsis Mitra Utama untuk mempelajari pembuatan web app dan web api menggunakan c# dan teknologi .Net. Project ini menggunakan .Net 6 Framework dengan bantuan library swagger untuk membuat Rest API nya. Dalam pembangunan project ini saya mempelajari pengimplementasian Bootstrap, HTML dan CSS pada tampilan antarmuka Web App dan membuat method GET, PUT, POST, DELETE untuk REST API.

Database yang digunakan dalam projek ini menggunakan Microsoft SQL Server. Selain itu pembuatan web app dan api menggunakan Visual Studio.

Berikut merupakan beberapa tampilan web app.

![Screenshot](./category.png)
![Screenshot](./product.png)

## Tahap memulai untuk menyambungkan repo dengan local

```
cd existing_repo
git remote add origin https://gitlab.com/elizams1/xpos_porto.git
git branch -M main
git push -uf origin main
```

## Tahap untuk memperbaharui file ke repo

```
git add .
git commit -m "<message>"
git push
```
