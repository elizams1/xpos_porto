﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using System.Security.Claims;
using XPOS336.Models;
using XPOS336.ViewModel;

namespace XPOS336.Controllers
{
    public class AuthController : Controller
    {
        private readonly CustomerModel customer;
        private readonly string imageFolder;

        public AuthController(IConfiguration _config)
        {
            customer = new CustomerModel(_config);
            imageFolder = _config["ImgFolder"];
        }

        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                HttpContext.Session.SetString("warnMsg", "Anda sudah login!");
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                HttpContext.Session.SetString("warnMsg", "Anda sudah login!");
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(VMTblMCustomer data)
        {
            if (User.Identity.IsAuthenticated)
            {
                HttpContext.Session.SetString("warnMsg", "Anda sudah login!");
                return RedirectToAction("Index", "Home");
            }

            VMResponse? response = customer.GetByEmail(data.Email);

            if (response != null)
            {
                if (response.statusCode == HttpStatusCode.OK)
                {
                    VMTblMCustomer customer = JsonConvert.DeserializeObject<VMTblMCustomer>(JsonConvert.SerializeObject(response.data));

                    if(data.Password == customer.Password)
                    {
                        HttpContext.Session.SetInt32("custId", (int)customer.Id);
                        HttpContext.Session.SetString("custName", customer.Name);
                        HttpContext.Session.SetString("custEmail",customer.Email);
                        HttpContext.Session.SetInt32("custRoleId", (int)(customer.RoleId??0));

                        var claims = new List<Claim>
                        {
                            new Claim(ClaimTypes.Role, customer.RoleId.ToString()),
                        };
                        var identity = new ClaimsIdentity(claims, "CookieAuth");
                        var principal = new ClaimsPrincipal(identity);
                        await HttpContext.SignInAsync("CookieAuth", principal);

                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        HttpContext.Session.SetString("warnMsg", "Salah Password");
                        
                    }
                }
                else
                {
                    HttpContext.Session.SetString("warnMsg", "Email dan Password Tidak Terdaftar");
                }
            }
            
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync("CookieAuth");
            HttpContext.Session.Clear();
            HttpContext.Session.SetString("warnMsg", "Anda sudah keluar!");
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Profile(int custId)
        {
            VMTblMCustomer? data = customer.GetById(custId);
            ViewBag.ImageFolder = imageFolder;

            return View(data);
        }

        
        public IActionResult Register()
        {
            if (User.Identity.IsAuthenticated)
            {
                HttpContext.Session.SetString("warnMsg", "Anda sudah login!");
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
    }
}
