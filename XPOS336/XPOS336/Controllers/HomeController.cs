﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using XPOS336.Models;

namespace XPOS336.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            HttpContext.Session.SetString("infoMsg", "Ini value session dari key InfoMsg");

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult AccessDenied()
        {
            HttpContext.Session.SetString("errMsg", "Access Denied!");

            return RedirectToAction("Index");
        }
    }
}
