﻿using Microsoft.AspNetCore.Mvc;

namespace XPOS336.Controllers
{
    public class TestController : Controller
    {
        private string ImgFolder;
        public TestController(IConfiguration _config) {
            ImgFolder = _config["ImgFolder"];
        } 

        public IActionResult Index()
        {
            //ViewBag.ImgFolder = ImgFolder;
            ViewData["ImgFolder"] = ImgFolder;
            return View();
        }
    }
}
