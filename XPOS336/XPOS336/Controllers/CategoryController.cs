﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using XPOS336.AddOns;
using XPOS336.Models;
using XPOS336.ViewModel;

namespace XPOS336.Controllers
{
    
    public class CategoryController : Controller
    {
       
        private readonly CategoryModel category;
        private VMResponse response = new VMResponse();

        private readonly int pageSize;


        public CategoryController(IConfiguration _config)
        {
            category = new CategoryModel(_config);
            pageSize = int.Parse(_config["PageSize"]);
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Index(string? filter, int? pageNumber, int? currPageSize)
        {


            List<VMTblMCategory>? data = new List<VMTblMCategory>();
            if (filter == null)
            {
                data = category.GetAll();
            }
            else
            {
                data = category.GetByFilter(filter);
            }
            ViewBag.Filter = filter;
            ViewBag.PageSize = currPageSize ?? pageSize;

            return View(Pagination<VMTblMCategory>.Create(data, pageNumber ?? 1, ViewBag.PageSize));
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Detail(int id)
        {
            VMTblMCategory? data = category.GetById(id);

            ViewBag.Title = "Category Detail";
            return View(data);
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Add()
        {
            ViewBag.Title = "Add Category";
            return View();
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        [HttpPost]
        public async Task <VMResponse> Add(VMTblMCategory data)
        {
            response = await category.CreateAsync(data);
            
            return response;
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Edit(int id)
        {
            VMTblMCategory? data = category.GetById(id);
            ViewBag.Title = "Edit Category";
            return View(data);
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        [HttpPost]
        public async Task<VMResponse> Edit(VMTblMCategory data)
        {
            response = await category.UpdateAsync(data);

            return response;
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Delete(int id)
        {
            ViewBag.Title = "Delete Category Confirmation";
            return View(id);
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        [HttpPost]
        public async Task<VMResponse> Delete(int id, int userId)
        {
            response = await category.DeleteAsync(id,userId);

            return response;
        }
    }
}
