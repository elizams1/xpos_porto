﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using XPOS336.AddOns;
using XPOS336.Models;
using XPOS336.ViewModel;

namespace XPOS336.Controllers
{
    public class VariantController : Controller
    {
        private readonly VariantModel variant;
        private readonly CategoryModel category;
        private readonly int pageSize;
        private VMResponse response = new VMResponse();

        public VariantController(IConfiguration _config)
        {
            variant = new VariantModel(_config);
            category = new CategoryModel(_config);
            pageSize = int.Parse(_config["PageSize"]);
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Index(string? filter, int? pageNumber, int? currPageSize)
        {
            List<VMTblMVariant> data = new List<VMTblMVariant>();

            if (filter == null)
            {
                data = variant.GetAll();
            }
            else
            {
                data = variant.GetByFilter(filter);
            }
            ViewBag.Filter = filter;
            ViewBag.PageSize = currPageSize ?? pageSize;

            return View(Pagination<VMTblMVariant>.Create(data, pageNumber ?? 1, ViewBag.PageSize));
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Detail(int id)
        {
            VMTblMVariant? data = variant.GetById(id);

            ViewBag.Title = "Variant Detail";
            return View(data);
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Add()
        {
            ViewBag.Title = "Add Variant";
            ViewBag.Category = category.GetAll();
            return View();
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        [HttpPost]
        public async Task<VMResponse> Add(VMTblMVariant data)
        {
            response = await variant.CreateAsync(data);

            return response;
        }


        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Edit(int id)
        {
            VMTblMVariant? data = variant.GetById(id);
            ViewBag.Title = "Edit Variant";
            ViewBag.Category = category.GetAll();
            return View(data);
        }


        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        [HttpPost]
        public async Task<VMResponse> Edit(VMTblMVariant data)
        {
            response = await variant.UpdateAsync(data);

            return response;
        }


        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Delete(int id)
        {
            ViewBag.Title = "Delete Variant Confirmation";
            return View(id);
        }


        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        [HttpPost]
        public async Task<VMResponse> Delete(int id, int userId)
        {
            response = await variant.DeleteAsync(id, userId);

            return response;
        }
    }
}
