﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using XPOS336.AddOns;
using XPOS336.Models;
using XPOS336.ViewModel;

namespace XPOS336.Controllers
{
    public class ProductController : Controller
    {
        private readonly ProductModel product;
        private readonly VariantModel variant;
        private readonly CategoryModel category;
        private readonly int pageSize;
        private string? imageFolder;

        private VMResponse response = new VMResponse();


        public ProductController(IConfiguration _config, IWebHostEnvironment _webHostEnv)
        {
            product = new ProductModel(_config, _webHostEnv);
            variant = new VariantModel(_config);
            category = new CategoryModel(_config);
            imageFolder = _config["ImgFolder"];
            pageSize = int.Parse(_config["PageSize"]);
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public List<VMTblMVariant>? GetVariantByCategoryId(int catId)
        {
            return variant?.GetAll().Where(v => v.CategoryId == catId).ToList();
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Index(string? filter, string? orderBy, int? pageNumber, int? currPageSize)
        {
            List<VMTblMProduct>? data = new List<VMTblMProduct>();
            if(filter == null)
            {
                data = product.GetAll();
            }
            else
            {
                data = product.GetByFilter(filter);
            }
            ViewBag.Filter = filter;
            ViewBag.ImageFolder = imageFolder;
            ViewBag.OrderBy = orderBy;
            ViewBag.PageSize = currPageSize ?? pageSize;


            return View(Pagination<VMTblMProduct>.Create(data, pageNumber ?? 1, ViewBag.PageSize));
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Detail(int id)
        {
            VMTblMProduct? data = product.GetById(id);

            ViewBag.Title = "Product Detail";
            ViewBag.ImageFolder = imageFolder;
            return View(data);
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Add()
        {
            ViewBag.Title = "Add Product";
            ViewBag.Category = category.GetAll();
            ViewBag.Variant = variant.GetAll();


            return View();
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        [HttpPost]
        public async Task<VMResponse?> AddAsync(VMTblMProduct formData)
        {
            try
            {

                response = await product.CreateAsync(formData);

            }
            catch
            {

            }
            return response;
        }


        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Edit(int id)
        {
            VMTblMProduct? data = product.GetById(id);
            ViewBag.Title = "Edit Product";
            ViewBag.Category = category.GetAll();
            ViewBag.ImageFolder = imageFolder;
            return View(data);
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        [HttpPost]
        public async Task<VMResponse> Edit(VMTblMProduct formData)
        {
            VMTblMProduct? data;

            try
            {
                response = await product.UpdateAsync(formData);
            }
            catch
            {

            }

            return response;
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Delete(int id)
        {
            ViewBag.Title = "Delete Product Confirmation";
            return View(id);
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        [HttpPost]
        public async Task<VMResponse> Delete(int id, int userId)
        {
            

            try
            {
                response = await product.DeleteAsync(id,userId);
            }
            catch
            {

            }

            return response;
        }
    }
}
