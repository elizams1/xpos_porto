﻿using Microsoft.AspNetCore.Mvc;
using XPOS336.Models;
using XPOS336.ViewModel;

namespace XPOS336.Controllers
{
    public class OrderController : Controller
    {
        private readonly OrderModel order;
        private readonly ProductModel product;
        private string? imageFolder;
        private VMResponse response = new VMResponse();

        public OrderController(IConfiguration _config,IWebHostEnvironment _webHostEnv)
        {
            order = new OrderModel(_config);
            product = new ProductModel(_config, _webHostEnv);
            imageFolder = _config["ImgFolder"];
        }
        public IActionResult Index()
        {
            List<VMTblMProduct>? data = new List<VMTblMProduct>();

            data = product.GetAll();

            ViewBag.ImageFolder = imageFolder;

            return View(data);
        }

        public IActionResult Details(List<VMTblTOrderDetail>? listCart, int totProduct, decimal estPrice)
        {
            ViewBag.Title = "Details Order";
            ViewBag.TotalProduct = totProduct;
            ViewBag.EstimatedPrice = estPrice;

            return View(listCart);
        }


        [HttpPost]
        public async Task <VMResponse?> Save(List<VMTblTOrderDetail>? listCart, int totProduct, decimal estPrice, int createBy, int custId)
        {
            VMTblTOrder data = new VMTblTOrder()
            {
                
                orderDetail = listCart,
                TotalQty = totProduct,
                Amount = estPrice,
                IsCheckout = false,
                CreateBy = createBy,
                CustomerId = custId

            };

            response = await order.CreateUpdateAsync(data);
            return response;
        }


        [HttpPost]
        public async Task <VMResponse?> CheckedOut(List<VMTblTOrderDetail>? listCart, int totProduct, decimal estPrice, int createBy, int custId)
        {
            VMTblTOrder data = new VMTblTOrder()
            {
                orderDetail = listCart,
                TotalQty = totProduct,
                Amount = estPrice,
                IsCheckout = true,
                CreateBy = createBy,
                CustomerId = custId

            };
            response = await order.CreateUpdateAsync(data);
            return response;
        }

        public IActionResult History(string? filter)
        {
            List<VMTblTOrder>? data = new List<VMTblTOrder>();
            if (filter == null)
            {
                data = order.GetAll();
            }
            else
            {
                data = order.GetByFilter(filter);
            }
            ViewBag.Filter = filter;

            return View(data);
        }

        public IActionResult OrderDetails(int orderHeaderId)
        {

            ViewBag.Title = "Detail History Order";
            return View();
        }

        [HttpPost]
        public async Task<VMResponse?> CheckedOutEdit(int orderHeaderId, int updateBy, bool isDeleted=false)
        {
            VMTblTOrder data = order.GetById(orderHeaderId);
            

            if(data != null)
            {
                data.UpdatedBy = updateBy;
                data.IsCheckout = (isDeleted ?false:true);
                data.IsDeleted = isDeleted;
                response = await order.CreateUpdateAsync(data);
            }
            else
            {
                response = new VMResponse();
                response.statusCode = System.Net.HttpStatusCode.NoContent;
                response.message = "Order does not exist";

            }
            

            return response;
        }
    }
}
