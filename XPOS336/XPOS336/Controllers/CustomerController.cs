﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using XPOS336.AddOns;
using XPOS336.Models;
using XPOS336.ViewModel;

namespace XPOS336.Controllers
{
    public class CustomerController : Controller
    {
        private readonly CustomerModel customer;
        private readonly int pageSize;
        private VMResponse response = new VMResponse();

        public CustomerController(IConfiguration _config)
        {
            customer = new CustomerModel(_config);
            pageSize = int.Parse(_config["PageSize"]);
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Index(string? filter, int? pageNumber, int? currPageSize)
        {
            List<VMTblMCustomer>? data = new List<VMTblMCustomer>();
            if (filter == null)
            {
                data = customer.GetAll();
            }
            else
            {
                data = customer.GetByFilter(filter);
            }
            ViewBag.Filter = filter;
            ViewBag.PageSize = currPageSize ?? pageSize;

            return View(Pagination<VMTblMCustomer>.Create(data, pageNumber ?? 1, ViewBag.PageSize));
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Detail(int id)
        {
            VMTblMCustomer? data = customer.GetById(id);

            ViewBag.Title = "Customer Detail";
            return View(data);
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Add()
        {
            ViewBag.Title = "Add Customer";
            return View();
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        [HttpPost]
        public async Task<VMResponse> Add(VMTblMCustomer data)
        {
            response = await customer.CreateAsync(data);

            return response;
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Edit(int id)
        {
            VMTblMCustomer? data = customer.GetById(id);
            ViewBag.Title = "Edit Customer";
            return View(data);
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        [HttpPost]
        public async Task<VMResponse> Edit(VMTblMCustomer data)
        {
            response = await customer.UpdateAsync(data);

            return response;
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        public IActionResult Delete(int id)
        {
            ViewBag.Title = "Delete Customer Confirmation";
            return View(id);
        }

        [Authorize(Roles = "1", AuthenticationSchemes = "CookieAuth")]
        [HttpPost]
        public async Task<VMResponse> Delete(int id, int userId)
        {
            response = await customer.DeleteAsync(id, userId);

            return response;
        }
    }
}
