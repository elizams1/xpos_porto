﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using System.Text;
using XPOS336.ViewModel;

namespace XPOS336.Models
{
    public class OrderModel
    {
        private readonly string apiUrl;
        private readonly HttpClient httpClient = new HttpClient();

        private string? jsonData;
        private VMResponse? apiResponse;
        private HttpContent content;

        public OrderModel(IConfiguration _config)
        {
            apiUrl = _config["apiUrl"];
        }

        public async Task<VMResponse?> CreateUpdateAsync(VMTblTOrder data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8,"application/json");

                if(data.Id == null)
                {
                    apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await httpClient.PostAsync(apiUrl + "/Order", content).Result.Content.ReadAsStringAsync()
                    );
                }
                else
                {
                    apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await httpClient.PutAsync(apiUrl + "/Order", content).Result.Content.ReadAsStringAsync()
                    );
                }
                

                if (apiResponse == null)
                {
                    throw new Exception("Order API cannot be reached");
                }
                

            }
            catch (Exception e)
            {
                apiResponse.statusCode = HttpStatusCode.InternalServerError;
                apiResponse.message = e.Message;
            }
            return apiResponse;
        }



        public List<VMTblTOrder>? GetAll() {
            List<VMTblTOrder>? data = null;

            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(httpClient.GetStringAsync(apiUrl + "/Order").Result);

                if(apiResponse != null)
                {
                    if(apiResponse.statusCode == HttpStatusCode.OK)
                    {
                        data = JsonConvert.DeserializeObject<List<VMTblTOrder>?>(JsonConvert.SerializeObject(apiResponse.data));
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                    
                }
                else
                {
                    throw new Exception("Order api cannot be reached!");
                }
                
            }
            catch (Exception e)
            {
                
            }
            return data;
        }

        public List<VMTblTOrder>? GetByFilter(string filter)
        {
            List<VMTblTOrder>? data = null;

            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(httpClient.GetStringAsync(apiUrl + "/Order/GetBy/"+filter).Result);

                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK)
                    {
                        data = JsonConvert.DeserializeObject<List<VMTblTOrder>?>(JsonConvert.SerializeObject(apiResponse.data));
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }

                }
                else
                {
                    throw new Exception("Order api cannot be reached!");
                }

            }
            catch (Exception e)
            {

            }
            return data;
        }

        public VMTblTOrder? GetById(int id)
        {
            VMTblTOrder? data = null;
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(httpClient.GetStringAsync(apiUrl + "/Order/Get/" + id).Result);

                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK)
                    {
                        data = JsonConvert.DeserializeObject<VMTblTOrder?>(JsonConvert.SerializeObject(apiResponse.data));
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("Order api cannot be reached!");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"[ProductModel-{DateTime.Now}]\n" + $"Message: {e.Message}\n" + $"InnerException: {e.InnerException}");
            }
            return data;
        }
    }
}
