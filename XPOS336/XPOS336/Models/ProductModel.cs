﻿using Newtonsoft.Json;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using XPOS336.ViewModel;

namespace XPOS336.Models
{
    public class ProductModel
    {
        private readonly string apiUrl;
        private readonly string imageFolder;
        private readonly HttpClient httpClient = new HttpClient();
        private VMResponse? apiResponse;
        private IWebHostEnvironment webHostEnv;
        private HttpContent content;
        private string jsonData;


        public ProductModel(IConfiguration _config, IWebHostEnvironment _webHostEnv)
        {
            apiUrl = _config["ApiUrl"];
            webHostEnv = _webHostEnv;
            imageFolder = _config["ImgFolder"];
        }

        public string? UploadFile(IFormFile? imageFile)
        {
            string uniqueFileName = "";
            string uploadFolder = "";
            if (imageFile != null)
            {

                uploadFolder = $"{webHostEnv.WebRootPath}\\{imageFolder}\\";
                uniqueFileName = $"{Guid.NewGuid()}-{imageFile.FileName}";

                //Upload Process
                using (FileStream fileStream = new FileStream($"{uploadFolder}{uniqueFileName}", FileMode.CreateNew))
                {
                    imageFile.CopyTo(fileStream);
                }

            }
            return uniqueFileName;
        }

        public List<VMTblMProduct>? GetAll()
        {
            List<VMTblMProduct>? data = null;

            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(httpClient.GetStringAsync(apiUrl + "/Product").Result);

                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK)
                    {
                        data = JsonConvert.DeserializeObject<List<VMTblMProduct>?>(JsonConvert.SerializeObject(apiResponse.data));
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("Product api cannot be reached!");
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<VMTblMProduct>? GetByFilter(string filter)
        {
            List<VMTblMProduct>? data = null;

            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(httpClient.GetStringAsync(apiUrl + "/Product/GetByFilter/" + filter).Result);

                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK)
                    {
                        data = JsonConvert.DeserializeObject<List<VMTblMProduct>?>(JsonConvert.SerializeObject(apiResponse.data));
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("Product api cannot be reached!");
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public VMTblMProduct? GetById(int id)
        {

            VMTblMProduct? data = null;

            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(httpClient.GetStringAsync(apiUrl + "/Product/Get/" + id).Result);

                if (apiResponse != null)
                {
                    if (apiResponse.statusCode == HttpStatusCode.OK)
                    {
                        data = JsonConvert.DeserializeObject<VMTblMProduct?>(JsonConvert.SerializeObject(apiResponse.data));
                    }
                    else
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    throw new Exception("Product api cannot be reached!");
                }
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public async Task<VMResponse?> CreateAsync(VMTblMProduct data)
        {
            try
            {
                if (data.ImageFile != null)
                {
                    data.Image = UploadFile(data.ImageFile);
                    data.ImageFile = null;
                }

                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (
                        await httpClient.PostAsync($"{apiUrl}/Product", content)
                    ).Content.ReadAsStringAsync()
                );

                if (apiResponse != null)
                {
                    if (apiResponse.statusCode != HttpStatusCode.Created || apiResponse.statusCode != HttpStatusCode.OK)
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    apiResponse.statusCode = HttpStatusCode.NotFound;
                    throw new ArgumentNullException("Product API cannot be reached");
                }
            }
            catch (Exception e)
            {
                

                Console.WriteLine($"[ProductModel-{DateTime.Now}]\n"+$"Message: {e.Message}\n" + $"InnerException: {e.InnerException}");
            }
            return apiResponse;
        }

        public async Task<VMResponse?> UpdateAsync(VMTblMProduct data)
        {
            try
            {
                if (data.ImageFile != null)
                {
                    if(data.Image != null)
                    {
                        DeleteOldImage(data.Image);
                    }

                    data.Image = UploadFile(data.ImageFile);
                    data.ImageFile = null;
                }


                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (
                        await httpClient.PutAsync($"{apiUrl}/Product", content)
                    ).Content.ReadAsStringAsync()
                );

                if (apiResponse != null)
                {
                    if (apiResponse.statusCode != HttpStatusCode.Created || apiResponse.statusCode != HttpStatusCode.OK)
                    {
                        throw new Exception(apiResponse.message);
                    }
                }
                else
                {
                    apiResponse.statusCode = HttpStatusCode.NotFound;
                    throw new ArgumentNullException("Product API cannot be reached");
                }
            }
            catch (Exception e)
            {

                Console.WriteLine($"[ProductModel-{DateTime.Now}]\n" + $"Message: {e.Message}\n" + $"InnerException: {e.InnerException}");
            }
            return apiResponse;
        }

        public bool DeleteOldImage(string oldImageFileName)
        {
            try
            {
                //Check if existing file is exist
                oldImageFileName = $"{webHostEnv.WebRootPath}\\{imageFolder}\\{oldImageFileName}";

                if (File.Exists(oldImageFileName))
                {
                    File.Delete(oldImageFileName);
                }
                else
                {
                    throw new ArgumentNullException("File does not exist");
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<VMResponse> DeleteAsync(int id, int userId)
        {
            try
            {
                DeleteOldImage(GetById(id).Image);

                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.DeleteAsync($"{apiUrl}/Product?id={id}&userId={userId}").Result.Content.ReadAsStringAsync());

                if (apiResponse == null)
                {
                    throw new Exception("Product API cannot be reached");
                }

            }
            catch(Exception e)
            {
                apiResponse.message += $"{e.Message}";
                apiResponse.data = null;
            }

            return apiResponse;
        }
    }
}
