﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XPOS336.DataModel
{
    [Table("Tbl_Coba_Terus")]
    public class TblCobaTerus
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string NameThree { get; set; }
        public string Keterangan { get; set; }
        public string JenisKelamin { get; set; }

    }
}
