﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace XPOS336.DataModel
{
    [Table("Tbl_Coba_Lagi")]
    public partial class TblCobaLagi
    {
        [Key]
        public int Id { get; set; }
        [StringLength(100)]
        public string NameTwo { get; set; } = null!;
        public string Description { get; set; } = null!;
        public string Type { get; set; } = null!;
    }
}
