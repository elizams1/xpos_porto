﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace XPOS336.DataModel
{
    [Table("Tbl_Coba_Terus")]
    public partial class TblCobaTeru
    {
        [Key]
        public int Id { get; set; }
        [StringLength(100)]
        public string NameThree { get; set; } = null!;
        public string Keterangan { get; set; } = null!;
        public string JenisKelamin { get; set; } = null!;
    }
}
