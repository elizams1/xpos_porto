﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace XPOS336.DataModel
{
    public partial class XPOS336Context : DbContext
    {
        public XPOS336Context()
        {
        }

        public XPOS336Context(DbContextOptions<XPOS336Context> options)
            : base(options)
        {
        }

        public virtual DbSet<TblCoba> TblCobas { get; set; } = null!;
        public virtual DbSet<TblCobaLagi> TblCobaLagis { get; set; } = null!;
        public virtual DbSet<TblCobaTeru> TblCobaTerus { get; set; } = null!;
        public virtual DbSet<TblMCategory> TblMCategories { get; set; } = null!;
        public virtual DbSet<TblMCustomer> TblMCustomers { get; set; } = null!;
        public virtual DbSet<TblMProduct> TblMProducts { get; set; } = null!;
        public virtual DbSet<TblMVariant> TblMVariants { get; set; } = null!;
        public virtual DbSet<TblTOrderDetail> TblTOrderDetails { get; set; } = null!;
        public virtual DbSet<TblTOrderHeader> TblTOrderHeaders { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=localhost;Initial Catalog=XPOS336;user id=sa;Password=P@ssw0rd");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TblMCategory>(entity =>
            {
                entity.Property(e => e.CreateDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblMCustomer>(entity =>
            {
                entity.Property(e => e.CreateDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblMProduct>(entity =>
            {
                entity.Property(e => e.CreateDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.Price).HasDefaultValueSql("((0))");

                entity.Property(e => e.Stock).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblMVariant>(entity =>
            {
                entity.Property(e => e.CreateDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.TblMVariants)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Tbl_M_Variant_Tbl_M_Categories");
            });

            modelBuilder.Entity<TblTOrderDetail>(entity =>
            {
                entity.Property(e => e.CreateDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.OrderHeader)
                    .WithMany(p => p.TblTOrderDetails)
                    .HasForeignKey(d => d.OrderHeaderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Order_Detail_Tbl_T_Order_Header");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.TblTOrderDetails)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Order_Detail_Tbl_M_Product");
            });

            modelBuilder.Entity<TblTOrderHeader>(entity =>
            {
                entity.Property(e => e.CreateDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
