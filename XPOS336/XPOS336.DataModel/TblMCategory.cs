﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace XPOS336.DataModel
{
    [Table("Tbl_M_Category")]
    public partial class TblMCategory
    {
        public TblMCategory()
        {
            TblMVariants = new HashSet<TblMVariant>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("category_name")]
        [StringLength(100)]
        [Unicode(false)]
        public string CategoryName { get; set; } = null!;
        [Column("description")]
        [Unicode(false)]
        public string? Description { get; set; }
        [Column("is_deleted")]
        public bool? IsDeleted { get; set; }
        [Column("create_by")]
        public int CreateBy { get; set; }
        [Column("create_date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("update_date", TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }

        [InverseProperty("Category")]
        public virtual ICollection<TblMVariant> TblMVariants { get; set; }
    }
}
