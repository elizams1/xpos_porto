﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace XPOS336.DataModel
{
    [Table("Tbl_M_Product")]
    public partial class TblMProduct
    {
        public TblMProduct()
        {
            TblTOrderDetails = new HashSet<TblTOrderDetail>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        [StringLength(100)]
        [Unicode(false)]
        public string Name { get; set; } = null!;
        [Column("price", TypeName = "decimal(18, 0)")]
        public decimal? Price { get; set; }
        [Column("stock")]
        public int? Stock { get; set; }
        [Column("variant_id")]
        public int VariantId { get; set; }
        [Column("image")]
        [Unicode(false)]
        public string? Image { get; set; }
        [Column("is_deleted")]
        public bool? IsDeleted { get; set; }
        [Column("create_by")]
        public int? CreateBy { get; set; }
        [Column("create_date", TypeName = "datetime")]
        public DateTime? CreateDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("update_date", TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }

        [InverseProperty("Product")]
        public virtual ICollection<TblTOrderDetail> TblTOrderDetails { get; set; }
    }
}
