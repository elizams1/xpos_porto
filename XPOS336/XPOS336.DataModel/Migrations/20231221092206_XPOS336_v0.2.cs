﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XPOS336.DataModel.Migrations
{
    public partial class XPOS336_v02 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Tbl_Coba_Terus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NameThree = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Keterangan = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    JenisKelamin = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tbl_Coba_Terus", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tbl_Coba_Terus");
        }
    }
}
