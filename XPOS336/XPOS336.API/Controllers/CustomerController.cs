﻿using Microsoft.AspNetCore.Mvc;
using XPOS336.DataAccess;
using XPOS336.DataModel;
using XPOS336.ViewModel;

namespace XPOS336.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class CustomerController : Controller
    {
        private DACustomer customer;
        public CustomerController(XPOS336Context _db)
        {
            customer = new DACustomer(_db);
        }

        [HttpGet]
        public VMResponse GetAll() => customer.GetByFilter();

        [HttpGet("[action]/{id?}")]
        public VMResponse Get(int id) => customer.GetById(id);

        [HttpGet("[action]/{filter?}")]
        public VMResponse GetByFilter(string filter) => customer.GetByFilter(filter);

        [HttpGet("[action]/{email?}")]
        public VMResponse GetByEmail(string email) => customer.GetByEmail(email);


        [HttpPost]
        public VMResponse Create(VMTblMCustomer data) => customer.Create(data);

        [HttpPut]
        public VMResponse Update(VMTblMCustomer data) => customer.Update(data);

        [HttpDelete]
        public VMResponse Delete(int id, int userId) => customer.Delete(id, userId);
        
    }
}
