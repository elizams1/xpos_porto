﻿using Microsoft.AspNetCore.Mvc;
using XPOS336.DataAccess;
using XPOS336.DataModel;
using XPOS336.ViewModel;

namespace XPOS336.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private DAProduct product;
        public ProductController(XPOS336Context _db)
        {
            product = new DAProduct(_db);
        }


        [HttpGet]
        public VMResponse GetAll() => product.GetByFilter();

        [HttpGet("[action]/{id?}")]
        public VMResponse Get(int id) => product.GetById(id);

        [HttpGet("[action]/{filter?}")]
        public VMResponse GetByFilter(string filter) => product.GetByFilter(filter);

        [HttpPost]
        public VMResponse Create(VMTblMProduct data) => product.Create(data);

        [HttpPut]
        public VMResponse Update(VMTblMProduct data) => product.Update(data);
         

        [HttpDelete]
        public VMResponse Delete(int id, int userId) => product.Delete(id, userId);
    }
}
