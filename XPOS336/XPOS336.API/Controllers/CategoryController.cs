﻿using Microsoft.AspNetCore.Mvc;
using XPOS336.DataAccess;
using XPOS336.DataModel;
using XPOS336.ViewModel;

namespace XPOS336.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        private DACategory category;
        public CategoryController(XPOS336Context _db)
        {
            category = new DACategory(_db);
        }

        [HttpGet]
        public VMResponse GetAll() => category.GetByFilter();

        [HttpGet("[action]/{id?}")]
        public VMResponse Get(int id) => category.GetById(id);

        [HttpGet("[action]/{filter?}")]
        public VMResponse GetByFilter(string filter) => category.GetByFilter(filter);

        [HttpPost]
        public VMResponse Create(VMTblMCategory data) => category.Create(data);
        

        [HttpPut]
        public VMResponse Update(VMTblMCategory data) => category.Update(data);
 

        [HttpDelete]
        public VMResponse Delete(int id, int userId) => category.Delete(id, userId);
        
    }
}
