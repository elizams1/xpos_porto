﻿using Microsoft.AspNetCore.Mvc;
using XPOS336.DataAccess;
using XPOS336.DataModel;
using XPOS336.ViewModel;

namespace XPOS336.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    
    public class OrderController : Controller
    {
        private DAOrder order;
        public OrderController(XPOS336Context _db) 
        {
            order = new DAOrder(_db);
        }

        [HttpGet]
        public VMResponse GetAll() => order.GetByFilter();
        

        [HttpGet("[action]/{id?}")]
        public VMResponse Get(int id) => order.GetById(id);

        [HttpGet("[action]/{filter?}")]
        public VMResponse GetBy(string? filter) 
        {
            if (!string.IsNullOrEmpty(filter))
            {
                return order.GetByFilter(filter);
            }
            else
            {
                return new VMResponse()
                {
                    message = "Filter is null, Please insert the filter  for filter data by customer name or TRX Code!",
                    statusCode = System.Net.HttpStatusCode.BadRequest
                };
            }
        }

        [HttpPost]
        public VMResponse Create(VMTblTOrder data) => order.CreateUpdate(data);


        [HttpPut]
        public VMResponse Update(VMTblTOrder data) => order.CreateUpdate(data);

        //Delete tidak diimplementasikan pada order karena
    }
}
