﻿using Microsoft.AspNetCore.Mvc;
using XPOS336.DataAccess;
using XPOS336.DataModel;
using XPOS336.ViewModel;

namespace XPOS336.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VariantController : Controller
    {
        private DAVariant variant;

        public VariantController(XPOS336Context _db)
        {
            variant = new DAVariant(_db);
        }

        [HttpGet]
        public VMResponse GetAll() => variant.GetByFilter();

        [HttpGet("[action]/{id?}")]
        public VMResponse Get(int id) => variant.GetById(id);

        [HttpGet("[action]/{filter?}")]
        public VMResponse GetByFilter(string filter) => variant.GetByFilter(filter);

        [HttpPost]
        public VMResponse Create(VMTblMVariant data) => variant.Create(data);

        [HttpPut]
        public VMResponse Update(VMTblMVariant data) => variant.Update(data);

        [HttpDelete]
        public VMResponse Delete(int id, int userId) => variant.Delete(id, userId);
    }
}
