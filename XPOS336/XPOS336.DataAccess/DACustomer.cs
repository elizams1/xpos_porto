﻿using Microsoft.EntityFrameworkCore.Storage;
using System.Net;
using XPOS336.DataModel;
using XPOS336.ViewModel;

namespace XPOS336.DataAccess
{
    public class DACustomer
    {
        private VMResponse response = new VMResponse();
        private readonly XPOS336Context db;

        public DACustomer(XPOS336Context _db) { 
            db = _db;
        }

        public VMResponse GetById(int id)
        {
            try
            {
                if (id > 0)
                {
                    VMTblMCustomer? data = (
                       from c in db.TblMCustomers
                       where c.IsDeleted == false
                           && c.Id == id
                       select new VMTblMCustomer
                       {
                           Id = c.Id,
                           Name = c.Name,
                           Email = c.Email,
                           Password = c.Password,
                           Address = c.Address,
                           Phone = c.Phone,
                           RoleId = c.RoleId,

                           IsDeleted = c.IsDeleted,
                           CreateBy = c.CreateBy,
                           CreateDate = c.CreateDate,
                           UpdatedBy = c.UpdatedBy,
                           UpdateDate = c.UpdateDate
                       }
                   ).FirstOrDefault();

                    if (data != null)
                    {
                        response.data = data;
                        response.message = "Customer data successfully fetched";
                        response.statusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        response.message = "Customer data No Content";
                        response.statusCode = HttpStatusCode.NoContent;
                    }
                }
                else
                {
                    response.message = "Please input Customer id first";
                    response.statusCode = HttpStatusCode.BadRequest;
                }
            }
            catch (Exception e)
            {
                response.message = e.Message;
                response.statusCode = HttpStatusCode.NotFound;
            }

            return response;
        }

        public VMResponse GetByFilter(string filter)
        {
            try
            {
                List<VMTblMCustomer> data = (
                        from c in db.TblMCustomers
                        where  c.IsDeleted == false
                            && c.Name.Contains(filter ?? "")
                        select new VMTblMCustomer
                        {
                            Id = c.Id,
                            Name = c.Name,
                            Email = c.Email,
                            Password =  c.Password,
                            Address =   c.Address,
                            Phone = c.Phone,
                            RoleId =    c.RoleId,
                            
                            IsDeleted = c.IsDeleted,
                            CreateBy = c.CreateBy,
                            CreateDate = c.CreateDate,
                            UpdatedBy = c.UpdatedBy,
                            UpdateDate = c.UpdateDate
                        }
                    ).ToList();

                response.data = data;
                response.message = (data.Count > 0) ? $"{data.Count} Customer data successfully fetched" : "Customer has no data !";
                response.statusCode = (data.Count > 0) ? HttpStatusCode.OK : HttpStatusCode.NoContent;
            }
            catch (Exception e) { 
                response.message = e.Message;
                response.statusCode = HttpStatusCode.NotFound;
            }
            return response;    
        }

        public VMResponse GetByFilter() => GetByFilter("");

        public VMResponse Create(VMTblMCustomer data)
        {
            using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    TblMCustomer customer = new TblMCustomer();

                    customer.Name = data.Name;
                    customer.Email = data.Email;
                    customer.Password = data.Password;
                    customer.Address = data.Address;
                    customer.Phone = data.Phone;
                    customer.RoleId = data.RoleId;
                    
                    customer.IsDeleted = false;
                    customer.CreateBy = data.CreateBy;

                    //Proses Orm (Object Relational Mapping) -> insert DATA to DATABASE
                    db.Add(customer);
                    db.SaveChanges();

                    //Save Changes from Transaction to Database
                    dbTran.Commit();

                    response.data = customer;
                    response.message = "New Customer Data Succesfully Created";
                    response.statusCode = HttpStatusCode.Created;
                }
                catch (Exception e)
                {

                    //Undo Changes from Transaction
                    dbTran.Rollback();

                    response.data = data;
                    response.message = e.Message;
                    //karena defaultnya sudah internal server error maka tidak udah diberikan assign untuk statusCode
                }
            }
            return response;
        }

        //linq select yang query akan dijalankan pada sql server sedangkan Select akan dijalankan di memory C nya dan akan lebih lama karena tidak terfokuskan ke data

        public VMResponse Update(VMTblMCustomer data)
        {
            using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    VMTblMCustomer existingData = (VMTblMCustomer)GetById(data.Id ?? 0).data;

                    if (existingData != null)
                    {
                        TblMCustomer customer = new TblMCustomer()
                        {
                            Id = (int)existingData.Id!,
                            CreateBy = existingData.CreateBy,
                            CreateDate = (DateTime)existingData.CreateDate!,

                            Name = data.Name,
                            Email = data.Email,
                            Password = data.Password,
                            Address = data.Address,
                            Phone = data.Phone,
                            RoleId = data.RoleId,

                        
                            UpdatedBy = data.UpdatedBy,
                            UpdateDate = DateTime.Now,

                            IsDeleted = false
                        };

                        //Proses Orm (Object Relational Mapping) -> insert DATA to DATABASE
                        db.Update(customer);
                        db.SaveChanges();

                        //Save Changes from Transaction to Database
                        dbTran.Commit();

                        response.data = customer;
                        response.message = $"Customer Data With name={data.Name} Succesfully Updated";
                        response.statusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        response.data = data;
                        response.message = "Requested Customer Data Cannot Be Updated";
                        response.statusCode = HttpStatusCode.NotFound;
                    }

                }
                catch (Exception e)
                {
                    dbTran.Rollback();

                    response.data = data;
                    response.message = e.Message;
                }
                return response;
            }
        }

        public VMResponse Delete(int id, int userId)
        {
            if (id != 0 && userId != 0)
            {
                using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                        VMTblMCustomer existingData = (VMTblMCustomer)GetById(id).data;
                        if (existingData != null)
                        {
                            TblMCustomer customer = new TblMCustomer()
                            {
                                Id = (int)existingData.Id!,
                                CreateBy = existingData.CreateBy,
                                CreateDate = (DateTime)existingData.CreateDate!,

                                Name = existingData.Name,
                                Email = existingData.Email,
                                Password = existingData.Password,
                                Address = existingData.Address,
                                Phone = existingData.Phone,
                                RoleId = existingData.RoleId,


                                UpdatedBy =userId,
                                UpdateDate = DateTime.Now,

                                IsDeleted = true
                            };
                            //Proses Orm (Object Relational Mapping) -> insert DATA to DATABASE
                            db.Update(customer);
                            db.SaveChanges();

                            //Save Changes from Transaction to Database
                            dbTran.Commit();

                            response.data = customer;
                            response.message = $"Customer Data With name = {existingData.Name} Succesfully Remove / Deleted";
                            response.statusCode = HttpStatusCode.OK;
                        }
                        else
                        {
                            response.message = $"Customer data with ID {id} not found";
                            response.statusCode = HttpStatusCode.NotFound;
                        }
                    }
                    catch (Exception e)
                    {
                        dbTran.Rollback();

                        response.message = e.Message;
                    }
                }
            }
            else
            {
                response.message = "Please input customer id and user id first";
                response.statusCode = HttpStatusCode.BadRequest;
            }
            return response;
        }

        public VMResponse GetByEmail(string email)
        {
            try
            {
                VMTblMCustomer? data = (
                        from c in db.TblMCustomers
                        where c.IsDeleted == false
                            && c.Email.Contains(email ?? "")
                        select new VMTblMCustomer
                        {
                            Id = c.Id,
                            Name = c.Name,
                            Email = c.Email,
                            Password = c.Password,
                            Address = c.Address,
                            Phone = c.Phone,
                            RoleId = c.RoleId,

                            IsDeleted = c.IsDeleted,
                            CreateBy = c.CreateBy,
                            CreateDate = c.CreateDate,
                            UpdatedBy = c.UpdatedBy,
                            UpdateDate = c.UpdateDate
                        }
                    ).FirstOrDefault();

                if (data != null)
                {
                    response.data = data;
                    response.message = "Customer data successfully fetched";
                    response.statusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.message = "Customer data No Content";
                    response.statusCode = HttpStatusCode.NoContent;
                }

            }
            catch (Exception e)
            {
                response.message = e.Message;
                response.statusCode = HttpStatusCode.NotFound;
            }
            return response;
        }
    }
}
