﻿using Microsoft.EntityFrameworkCore.Storage;
using System.Net;
using XPOS336.DataModel;
using XPOS336.ViewModel;

namespace XPOS336.DataAccess
{
    public class DAProduct
    {
        private VMResponse response = new VMResponse();
        private readonly XPOS336Context db;

        public DAProduct(XPOS336Context _db) { 
            db = _db;
        }

        public VMResponse GetById(int id)
        {
            try
            {
                if (id > 0)
                {
                    VMTblMProduct? data = (
                        from p in db.TblMProducts
                        join v in db.TblMVariants
                        on p.VariantId equals v.Id
                        join c in db.TblMCategories
                        on v.CategoryId equals c.Id
                        where p.IsDeleted == false
                            && c.IsDeleted == false
                            && v.IsDeleted == false
                            && p.Id == id
                        select new VMTblMProduct
                        {
                            Id = p.Id,
                            Name = p.Name,
                            Price = p.Price,
                            Stock = p.Stock,
                            Image = p.Image,

                            CategoryId = c.Id,
                            CategoryName = c.CategoryName,

                            VariantId = v.Id,
                            VariantName = v.Name,

                            IsDeleted = p.IsDeleted,
                            CreateBy = p.CreateBy,
                            CreateDate = p.CreateDate,
                            UpdatedBy = p.UpdatedBy,
                            UpdateDate = p.UpdateDate
                        }
                    ).FirstOrDefault();

                    if (data != null)
                    {
                        response.data = data;
                        response.message = "Product data successfully fetched";
                        response.statusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        response.message = "Product data No Content";
                        response.statusCode = HttpStatusCode.NoContent;
                    }

                    data = null;
                }
                else
                {
                    response.message = "Please input product id first";
                    response.statusCode = HttpStatusCode.BadRequest;
                }
                
            }
            catch (Exception e)
            {
                response.message = e.Message;
                response.statusCode = HttpStatusCode.NotFound;
            }
            return response;
        }

        public VMResponse GetByFilter(string filter)
        {
            try
            {
                List<VMTblMProduct> data = (
                        from p in db.TblMProducts
                        join v in db.TblMVariants
                            on p.VariantId equals v.Id
                        join c in db.TblMCategories
                            on v.CategoryId equals c.Id
                        where p.IsDeleted == false
                            && c.IsDeleted == false
                            && v.IsDeleted == false
                            && (
                            p.Name.Contains(filter ?? "")
                            || c.CategoryName.Contains(filter ?? "") 
                            || v.Name.Contains(filter ?? ""))
                        select new VMTblMProduct
                        {
                            Id = p.Id,
                            Name = p.Name,
                            Price = p.Price,
                            Stock = p.Stock,
                            Image = p.Image,

                            CategoryId = c.Id,
                            CategoryName = c.CategoryName,

                            VariantId = v.Id,
                            VariantName = v.Name,

                            IsDeleted = p.IsDeleted,
                            CreateBy = p.CreateBy,
                            CreateDate = p.CreateDate,
                            UpdatedBy = p.UpdatedBy,
                            UpdateDate = p.UpdateDate
                        }
                    ).ToList();

                response.data = data;
                response.message = (data.Count > 0) ? $"{data.Count} Product data successfully fetched" : "Product has no data !";
                response.statusCode = (data.Count > 0) ? HttpStatusCode.OK : HttpStatusCode.NoContent;

                data = null;
            }
            catch (Exception e) { 
                response.message = e.Message;
                response.statusCode = HttpStatusCode.NotFound;
            }
            return response;    
        }

        public VMResponse GetByFilter() => GetByFilter("");

        public VMResponse Create(VMTblMProduct data)
        {
            using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    VMTblMVariant? existVariant = (
                        from v in db.TblMVariants 
                        where v.Id == data.VariantId && v.IsDeleted==false
                        select new VMTblMVariant 
                        { 
                            Name = v.Name,
                            Id = v.Id
                        }
                        ).FirstOrDefault();

                    if ( existVariant != null )
                    {
                        TblMProduct product = new TblMProduct();

                        product.Name = data.Name;
                        product.Price = data.Price;
                        product.Stock = data.Stock;

                        product.VariantId = data.VariantId;
                        product.Image = data.Image;

                        product.IsDeleted = false;
                        product.CreateBy = data.CreateBy;

                        //Proses Orm (Object Relational Mapping) -> insert DATA to DATABASE
                        db.Add(product);
                        db.SaveChanges();

                        //Save Changes from Transaction to Database
                        dbTran.Commit();

                        response.data = product;
                        response.message = "New Product Data Succesfully Created";
                        response.statusCode = HttpStatusCode.Created;
                    }
                    else
                    {
                        response.message = "Variant id not found";
                        response.statusCode = HttpStatusCode.NotFound;
                    }
                    
                }
                catch (Exception e)
                {

                    //Undo Changes from Transaction
                    dbTran.Rollback();

                    response.data = data;
                    response.message = e.Message;
                    //karena defaultnya sudah internal server error maka tidak udah diberikan assign untuk statusCode
                }
            }
            return response;
        }

        public VMResponse Update(VMTblMProduct data)
        {
            using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    VMTblMProduct existingData = (VMTblMProduct)GetById(data.Id ?? 0).data;

                    if (existingData != null)
                    {
                        TblMProduct product = new TblMProduct()
                        {
                            Id = (int)existingData.Id,
                            CreateBy = existingData.CreateBy,
                            CreateDate = (DateTime)existingData.CreateDate,

                            Name = data.Name,
                            Price = data.Price,
                            Stock = data.Stock,
                            VariantId = data.VariantId,
                            Image = data.Image,

                            UpdatedBy = data.UpdatedBy,
                            UpdateDate = DateTime.Now,

                            IsDeleted = false
                        };

                        //Proses Orm (Object Relational Mapping) -> insert DATA to DATABASE
                        db.Update(product);
                        db.SaveChanges();

                        //Save Changes from Transaction to Database
                        dbTran.Commit();

                        response.data = product;
                        response.message = $"Product Data With name = {data.Name} Succesfully Updated";
                        response.statusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        response.data = data;
                        response.message = "Requested Product Data Cannot Be Updated";
                        response.statusCode = HttpStatusCode.NotFound;
                    }

                }
                catch (Exception e)
                {
                    dbTran.Rollback();

                    response.data = data;
                    response.message = e.Message;
                }
                return response;
            }
        }

        public VMResponse Delete(int id, int userId)
        {
            if (id != 0 && userId != 0)
            {
                using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                        VMTblMProduct existingData = (VMTblMProduct)GetById(id).data;
                        if (existingData != null)
                        {
                            TblMProduct product = new TblMProduct()
                            {
                                Id = (int)existingData.Id,
                                CreateBy = existingData.CreateBy,
                                CreateDate = (DateTime)existingData.CreateDate,

                                Name = existingData.Name,
                                Price = existingData.Price,
                                Stock = existingData.Stock,
                                VariantId = existingData.VariantId,
                                Image = existingData.Image,

                                UpdatedBy = userId,
                                UpdateDate = DateTime.Now,

                                IsDeleted = true
                            };
                            //Proses Orm (Object Relational Mapping) -> insert DATA to DATABASE
                            db.Update(product);
                            db.SaveChanges();

                            //Save Changes from Transaction to Database
                            dbTran.Commit();

                            response.data = product;
                            response.message = $"Product Data With name = {existingData.Name} Succesfully Remove / Deleted";
                            response.statusCode = HttpStatusCode.OK;
                        }
                        else
                        {
                            response.message = $"Product data with ID {id} not found";
                            response.statusCode = HttpStatusCode.NotFound;
                        }
                    }
                    catch (Exception e)
                    {
                        dbTran.Rollback();

                        response.message = e.Message;
                    }
                }
            }
            else
            {
                response.message = "Please input product id and user id first";
                response.statusCode = HttpStatusCode.BadRequest;
            }
            return response;
        }
    }
}
