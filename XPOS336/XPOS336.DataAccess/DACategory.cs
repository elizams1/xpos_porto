﻿using Microsoft.EntityFrameworkCore.Storage;
using System.Net;
using XPOS336.DataModel;
using XPOS336.ViewModel;

namespace XPOS336.DataAccess
{
    public class DACategory
    {
        private VMResponse response = new VMResponse();
        private readonly XPOS336Context db;

        public DACategory(XPOS336Context _db) { 
            db = _db;
        }

        public VMResponse GetById(int id)
        {
            try
            {
                if (id > 0)
                {
                    VMTblMCategory? data = (
                        from c in db.TblMCategories
                        where c.IsDeleted == false && c.Id == id
                        select new VMTblMCategory
                        {
                            Id = c.Id,
                            CategoryName = c.CategoryName,
                            Description = c.Description,

                            IsDeleted = c.IsDeleted,
                            CreateBy = c.CreateBy,
                            CreateDate = c.CreateDate,
                            UpdatedBy = c.UpdatedBy,
                            UpdateDate = c.UpdateDate
                        }
                    ).FirstOrDefault();

                    if (data != null)
                    {
                        response.data = data;
                        response.message = "Category data successfully fetched";
                        response.statusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        response.message = "Category data No Content";
                        response.statusCode = HttpStatusCode.NoContent;
                    }
                }
                else
                {
                    response.message = "Please input category id first";
                    response.statusCode = HttpStatusCode.BadRequest;
                }
            }
            catch (Exception e)
            {
                response.message = e.Message;
                response.statusCode = HttpStatusCode.NotFound;
            }

            return response;
        }

        public VMResponse GetByFilter(string filter)
        {
            try
            {
                List<VMTblMCategory> data = (
                    from c in db.TblMCategories
                    where  c.IsDeleted == false
                        && (c.CategoryName.Contains(filter ?? "") || c.Description.Contains(filter ?? ""))
                    select new VMTblMCategory
                    {
                        Id = c.Id,
                        CategoryName = c.CategoryName,
                        Description = c.Description,
                            
                        IsDeleted = c.IsDeleted,
                        CreateBy = c.CreateBy,
                        CreateDate = c.CreateDate,
                        UpdatedBy = c.UpdatedBy,
                        UpdateDate = c.UpdateDate
                    }
                ).ToList();

                response.data = data;
                response.message = (data.Count > 0) ? $"{data.Count} Category data successfully fetched" : "Category has no data !";
                response.statusCode = (data.Count > 0) ? HttpStatusCode.OK : HttpStatusCode.NoContent;
            }
            catch (Exception e) { 
                response.message = e.Message;
                response.statusCode = HttpStatusCode.NotFound;
            }
            return response;    
        }

        public VMResponse GetByFilter() => GetByFilter("");

        public VMResponse Create(VMTblMCategory data)
        {
            using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    TblMCategory category = new TblMCategory();

                    category.CategoryName = data.CategoryName;
                    category.Description = data.Description;    
                    category.IsDeleted = false;
                    category.CreateBy = data.CreateBy;

                    //Proses Orm (Object Relational Mapping) -> insert DATA to DATABASE
                    db.Add(category);
                    db.SaveChanges();

                    //Save Changes from Transaction to Database
                    dbTran.Commit();

                    response.data = category;
                    response.message = "New Category Data Succesfully Created";
                    response.statusCode = HttpStatusCode.Created;
                }
                catch (Exception e)
                {

                    //Undo Changes from Transaction
                    dbTran.Rollback();

                    response.data = data;
                    response.message = e.Message;
                    //karena defaultnya sudah internal server error maka tidak udah diberikan assign untuk statusCode
                }
            };
            
            return response;
        }

        public VMResponse Update(VMTblMCategory data)
        {
            using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    VMTblMCategory existingData = (VMTblMCategory)GetById(data.Id??0).data;

                    if (existingData != null)
                    {
                        TblMCategory category = new TblMCategory()
                        {
                            Id = (int)existingData.Id!,
                            CreateBy = existingData.CreateBy,
                            CreateDate = (DateTime)existingData.CreateDate!,


                            CategoryName = data.CategoryName,
                            Description = data.Description,
                            UpdatedBy = data.UpdatedBy,
                            UpdateDate = DateTime.Now,

                            IsDeleted = false
                        };

                        //Proses Orm (Object Relational Mapping) -> insert DATA to DATABASE
                        db.Update(category);
                        db.SaveChanges();

                        //Save Changes from Transaction to Database
                        dbTran.Commit();

                        response.data = category;
                        response.message = $"Category Data With name={data.CategoryName} Succesfully Updated";
                        response.statusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        response.data = data;
                        response.message = "Requested Category Data Cannot Be Updated";
                        response.statusCode = HttpStatusCode.NotFound;
                    }

                }
                catch (Exception e)
                {
                    dbTran.Rollback();

                    response.data = data;
                    response.message = e.Message;
                }

                return response;
            }
        }

        public VMResponse Delete(int id, int userId)
        {
            if(id!=0 && userId != 0)
            {
                using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                        VMTblMCategory existingData = (VMTblMCategory)GetById(id).data;
                        if(existingData!=null)
                        {
                            TblMCategory category = new TblMCategory()
                            {
                                Id = (int)existingData.Id!,
                                CreateBy = existingData.CreateBy,
                                CreateDate = (DateTime)existingData.CreateDate!,
                                CategoryName = existingData.CategoryName,
                                Description = existingData.Description,
                                
                                UpdatedBy = userId,
                                UpdateDate = DateTime.Now,

                                IsDeleted = true
                            };
                            //Proses Orm (Object Relational Mapping) -> insert DATA to DATABASE
                            db.Update(category);
                            db.SaveChanges();

                            //Save Changes from Transaction to Database
                            dbTran.Commit();

                            response.data = category;
                            response.message = $"Category Data With name = {existingData.CategoryName} Succesfully Remove / Deleted";
                            response.statusCode = HttpStatusCode.OK;
                        }
                        else
                        {
                            response.message = $"Category data with ID {id} not found";
                            response.statusCode=HttpStatusCode.NotFound;
                        }
                    }
                    catch (Exception e)
                    {
                        dbTran.Rollback();

                        response.message= e.Message;
                    }
                }
            }
            else
            {
                response.message = "Please input category id and user id first";
                response.statusCode = HttpStatusCode.BadRequest;
            }
            return response;
        }
        
    }
}
