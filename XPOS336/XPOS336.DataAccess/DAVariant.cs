﻿using Microsoft.EntityFrameworkCore.Storage;
using System.Net;
using XPOS336.DataModel;
using XPOS336.ViewModel;

namespace XPOS336.DataAccess
{
    public class DAVariant
    {
        private VMResponse response = new VMResponse();
        private readonly XPOS336Context db;

        public DAVariant(XPOS336Context _db) { 
            db = _db;
        }

        public VMResponse GetById(int id)
        {
            try
            {
                if (id > 0)
                {
                    VMTblMVariant? data = (
                        from v in db.TblMVariants
                        join c in db.TblMCategories
                          on v.CategoryId equals c.Id
                        where v.IsDeleted == false
                            && c.IsDeleted == false
                            && v.Id == id
                        select new VMTblMVariant
                        {
                            Id = v.Id,
                            Name = v.Name,
                            Description = v.Description,

                            CategoryId = v.CategoryId,
                            CategoryName = c.CategoryName,

                            IsDeleted = v.IsDeleted,
                            CreateBy = v.CreateBy,
                            CreateDate = v.CreateDate,
                            UpdatedBy = v.UpdatedBy,
                            UpdateDate = v.UpdateDate
                        }
                    ).FirstOrDefault();

                    if (data != null)
                    {
                        response.data = data;
                        response.message = "Variant data successfully fetched";
                        response.statusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        response.message = "Variant data No Content";
                        response.statusCode = HttpStatusCode.NoContent;
                    }
                }
                else
                {
                    response.message = "Please input Variant id first";
                    response.statusCode = HttpStatusCode.BadRequest;
                }
            }
            catch (Exception e)
            {
                response.message = e.Message;
                response.statusCode = HttpStatusCode.NotFound;
            }

            return response;
        }

        public VMResponse GetByFilter(string filter)
        {
            try
            {
                List<VMTblMVariant> data = (
                        from v in db.TblMVariants
                        join c in db.TblMCategories
                        on v.CategoryId equals c.Id
                        where  v.IsDeleted == false
                            && c.IsDeleted == false
                            && (v.Name.Contains(filter ?? "")
                            || v.Description.Contains(filter ?? "") 
                            || c.CategoryName.Contains(filter ?? ""))
                        select new VMTblMVariant
                        {
                            Id = v.Id,
                            Name = v.Name,
                            Description =  v.Description,

                            CategoryId = v.CategoryId,
                            CategoryName = c.CategoryName,

                            IsDeleted = v.IsDeleted,
                            CreateBy = v.CreateBy,
                            CreateDate = v.CreateDate,
                            UpdatedBy = v.UpdatedBy,
                            UpdateDate = v.UpdateDate
                        }
                    ).ToList();

                response.data = data;
                response.message = (data.Count > 0) ? $"{data.Count} Variant data successfully fetched" : "Variant has no data !";
                response.statusCode = (data.Count > 0) ? HttpStatusCode.OK : HttpStatusCode.NoContent;
            }
            catch (Exception e) { 
                response.message = e.Message;
                response.statusCode = HttpStatusCode.NotFound;
            }
            return response;    
        }

        public VMResponse GetByFilter()
        {
            return GetByFilter("");
        }

        public VMResponse Create(VMTblMVariant data)
        {
            using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    VMTblMCategory? existCategory = (
                        from c in db.TblMCategories
                        where c.Id == data.CategoryId
                                && c.IsDeleted == false
                        select new VMTblMCategory
                        {
                            CategoryName = c.CategoryName,
                            Id = c.Id
                        }).FirstOrDefault();
                    if (existCategory != null)
                    {
                        TblMVariant variant = new TblMVariant();

                        variant.CategoryId = data.CategoryId;
                        variant.Name = data.Name;
                        variant.Description = data.Description;

                        variant.IsDeleted = false;
                        variant.CreateBy = data.CreateBy;

                        //Proses Orm (Object Relational Mapping) -> insert DATA to DATABASE
                        db.Add(variant);
                        db.SaveChanges();

                        //Save Changes from Transaction to Database
                        dbTran.Commit();

                        response.data = variant;
                        response.message = "New Variant Data Succesfully Created";
                        response.statusCode = HttpStatusCode.Created;
                    }
                    else
                    {
                        response.message = "Category id not found";
                        response.statusCode = HttpStatusCode.NotFound;
                    }
                }
                catch (Exception e)
                {

                    //Undo Changes from Transaction
                    dbTran.Rollback();

                    response.data = data;
                    response.message = e.Message;
                    //karena defaultnya sudah internal server error maka tidak udah diberikan assign untuk statusCode
                }
            }
            return response;
        }

        public VMResponse Update(VMTblMVariant data)
        {
            using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    VMTblMVariant existingData = (VMTblMVariant )GetById(data.Id ?? 0).data;

                    if (existingData != null)
                    {
                        TblMVariant variant = new TblMVariant()
                        {
                            Id = (int)existingData.Id,
                            CreateBy = existingData.CreateBy,
                            CreateDate = (DateTime)existingData.CreateDate,

                            Name = data.Name,
                            Description = data.Description,
                            CategoryId = data.CategoryId,

                            UpdatedBy = data.UpdatedBy,
                            UpdateDate = DateTime.Now,

                            IsDeleted = false
                        };

                        //Proses Orm (Object Relational Mapping) -> insert DATA to DATABASE
                        db.Update(variant);
                        db.SaveChanges();

                        //Save Changes from Transaction to Database
                        dbTran.Commit();

                        response.data = variant;
                        response.message = $"Variant Data With name = {data.Name} Succesfully Updated";
                        response.statusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        response.data = data;
                        response.message = "Requested Variant Data Cannot Be Updated";
                        response.statusCode = HttpStatusCode.NotFound;
                    }

                }
                catch (Exception e)
                {
                    dbTran.Rollback();

                    response.data = data;
                    response.message = e.Message;
                }
                return response;
            }
        }

        public VMResponse Delete(int id, int userId)
        {
            if (id != 0 && userId != 0)
            {
                using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                        VMTblMVariant existingData = (VMTblMVariant )GetById(id).data;
                        if (existingData != null)
                        {
                            TblMVariant variant = new TblMVariant()
                            {
                                Id = (int)existingData.Id,
                                CreateBy = existingData.CreateBy,
                                CreateDate = (DateTime)existingData.CreateDate,

                                Name = existingData.Name,
                                Description = existingData.Description,
                                CategoryId = existingData.CategoryId,

                                UpdatedBy = userId,
                                UpdateDate = DateTime.Now,

                                IsDeleted = true
                            };
                            //Proses Orm (Object Relational Mapping) -> insert DATA to DATABASE
                            db.Update(variant);
                            db.SaveChanges();

                            //Save Changes from Transaction to Database
                            dbTran.Commit();

                            response.data = variant;
                            response.message = $"Variant Data With name = {existingData.Name} Succesfully Remove / Deleted";
                            response.statusCode = HttpStatusCode.OK;
                        }
                        else
                        {
                            response.message = $"Variant data with ID {id} not found";
                            response.statusCode = HttpStatusCode.NotFound;
                        }
                    }
                    catch (Exception e)
                    {
                        dbTran.Rollback();

                        response.message = e.Message;
                    }
                }
            }
            else
            {
                response.message = "Please input variant id and user id first";
                response.statusCode = HttpStatusCode.BadRequest;
            }
            return response;
        }
    }
}
