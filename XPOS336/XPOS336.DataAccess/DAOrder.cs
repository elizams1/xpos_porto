﻿using Microsoft.EntityFrameworkCore.Storage;
using System.Net;
using XPOS336.DataModel;
using XPOS336.ViewModel;

namespace XPOS336.DataAccess
{
    public class DAOrder
    {
        private VMResponse response = new VMResponse();
        private readonly XPOS336Context db;

        public DAOrder(XPOS336Context _db) { 
            db = _db;
        }

        public VMResponse GetById(int id)
        {
            try
            {
                if (id > 0)
                {
                    VMTblTOrder? data = (
                        from oh in db.TblTOrderHeaders
                        join od in db.TblTOrderDetails
                            on oh.Id equals od.OrderHeaderId
                        join c in db.TblMCustomers
                            on oh.CustomerId equals c.Id
                        where oh.Id == id
                            && oh.IsDeleted == false
                            && od.IsDeleted == false
                            && c.IsDeleted == false
                        select new VMTblTOrder
                        {
                            Id = oh.Id,
                            TrxCode = oh.TrxCode,
                            Amount = oh.Amount,
                            TotalQty = oh.TotalQty,

                            IsCheckout = oh.IsCheckout,
                            IsDeleted = oh.IsDeleted,

                            CustomerId = oh.CustomerId,
                            CustomerName = c.Name,

                            CreateBy = oh.CreateBy,
                            CreateDate = oh.CreateDate,
                            UpdatedBy = oh.UpdatedBy,
                            UpdateDate = oh.UpdateDate,

                            orderDetail = (
                                from od in db.TblTOrderDetails
                                join p in db.TblMProducts 
                                    on od.ProductId equals p.Id
                                where od.OrderHeaderId == oh.Id 
                                    && od.IsDeleted == false
                                    && p.IsDeleted == false
                                select new VMTblTOrderDetail
                                {
                                    Id = od.Id,
                                    OrderHeaderId = od.OrderHeaderId,
                                    Qty = od.Qty,
                                    Price = od.Price,

                                    ProductId = od.ProductId,
                                    ProductName = p.Name,

                                    IsDeleted = od.IsDeleted,

                                    CreateBy = od.CreateBy,
                                    CreateDate = od.CreateDate,
                                    UpdatedBy = od.UpdatedBy,
                                    UpdateDate = od.UpdateDate

                                }).ToList()
                        }
                        ).FirstOrDefault();

                    if (data != null)
                    {
                        response.data = data;
                        response.message = $"Order data with orderID = {id} successfully fetched";
                        response.statusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        response.message = $"Order data with orderID = {id} No Content";
                        response.statusCode = HttpStatusCode.NoContent;
                    }

                    data = null;
                }
                else
                {
                    response.statusCode = HttpStatusCode.BadRequest;
                    throw new Exception("Please input orderID first");
                }
            }
            catch (Exception e)
            { 
                response.message = $"Order with orderID = {id} does not exist! {e.Message}";
            }
            return response;
        }
        
        public VMResponse GetByFilter(string filter)
        {
            try
            {
                List<VMTblTOrder> data = (
                        from oh in db.TblTOrderHeaders
                        join c in db.TblMCustomers
                            on oh.CustomerId equals c.Id
                        where (c.Name.Contains(filter??"") 
                            || oh.TrxCode.Contains(filter??""))
                            && oh.IsDeleted == false
                            && c.IsDeleted == false
                        select new VMTblTOrder
                        {
                            Id = oh.Id,
                            TrxCode = oh.TrxCode,
                            Amount = oh.Amount,
                            TotalQty = oh.TotalQty,

                            IsCheckout = oh.IsCheckout,
                            IsDeleted = oh.IsDeleted,

                            CustomerId = oh.CustomerId,
                            CustomerName = c.Name,

                            CreateBy = oh.CreateBy,
                            CreateDate = oh.CreateDate,
                            UpdatedBy = oh.UpdatedBy,
                            UpdateDate = oh.UpdateDate,

                            orderDetail = (
                                from od in db.TblTOrderDetails
                                join p in db.TblMProducts
                                    on od.ProductId equals p.Id
                                where od.OrderHeaderId == oh.Id
                                    && od.IsDeleted == false
                                    && p.IsDeleted == false 
                                select new VMTblTOrderDetail
                                {
                                    Id = od.Id,
                                    OrderHeaderId = od.OrderHeaderId,
                                    Qty = od.Qty,
                                    Price = od.Price,

                                    ProductId = od.ProductId,
                                    ProductName = p.Name,

                                    IsDeleted = od.IsDeleted,

                                    CreateBy = od.CreateBy,
                                    CreateDate = od.CreateDate,
                                    UpdatedBy = od.UpdatedBy,
                                    UpdateDate = od.UpdateDate

                                }).ToList()
                        }
                    ).ToList();

                if(data != null )
                {
                    response.data = data;
                    response.message = $"{data.Count} Variant data successfully fetched";
                    response.statusCode = HttpStatusCode.OK;
                }
                else
                {
                    throw new Exception("Order data no content !");
                }
            }
            catch (Exception e) {
                response.message = $"Order data does not exist! {e.Message}";
                response.statusCode = HttpStatusCode.NotFound;
            }
            return response;    
        }
        public VMResponse GetByFilter() => GetByFilter("");

        public VMResponse CreateUpdate(VMTblTOrder data)
        {
            string strStatus="";

            using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    TblTOrderHeader orderHeader = new TblTOrderHeader();
                    TblTOrderDetail orderDetail = new TblTOrderDetail();
                    VMTblTOrder? existingHeader = new VMTblTOrder();
                    VMTblTOrderDetail? existingDetail =  new VMTblTOrderDetail(); 
                    TblMProduct? product = new TblMProduct();
                    


                    orderHeader.IsDeleted = data.IsDeleted ?? false;

                    if (data.Id == 0 || data.Id == null)
                    {
                        //CREATE PROCESS
                        strStatus = "Created";

                        orderHeader.TrxCode = TrxCodeGenerator(
                            db.TblTOrderHeaders
                                .OrderByDescending(oh => oh.Id)
                                .Select(oh => oh.Id)
                                .FirstOrDefault()
                                );
                        orderHeader.CustomerId = data.CustomerId;
                        orderHeader.Amount = data.Amount;
                        orderHeader.TotalQty = data.TotalQty;
                        orderHeader.IsCheckout = data.IsCheckout;
                        orderHeader.CreateBy = data.CreateBy;
                        

                        db.Add(orderHeader);
                        db.SaveChanges();

                        foreach (VMTblTOrderDetail item in data.orderDetail)
                        {
                            orderDetail = new TblTOrderDetail();

                            orderDetail.OrderHeaderId = orderHeader.Id;

                            orderDetail.Qty = item.Qty;
                            orderDetail.Price = item.Price;
                            orderDetail.ProductId = item.ProductId;

                            orderDetail.IsDeleted = false;
                            orderDetail.CreateBy = orderHeader.CreateBy;
                            orderDetail.CreateDate = orderHeader.CreateDate;

                            db.Add(orderDetail);
                            db.SaveChanges();

                            //UPDATE PRODUCT STOCK
                            product = db.TblMProducts.Find(item.ProductId);

                            if (product != null)
                            {
                                //CHECK IF PRODUCT STOCK IS ENOUGH
                                if (product.Stock >= item.Qty)
                                {
                                    product.Stock -= item.Qty;

                                    //UPDATE PRODUCT STOCK DATA
                                    db.Update(product);
                                    db.SaveChanges();
                                }
                                else
                                {
                                    response.statusCode = HttpStatusCode.BadRequest;
                                    throw new Exception($"Product {item.ProductName} with ID = {item.ProductId} does not have enough stock");
                                }
                            }
                            else
                            {
                                response.statusCode = HttpStatusCode.NotFound;
                                throw new ArgumentNullException($"Product {item.ProductName} with ID = {item.ProductId} NOT FOUND");
                            }
                            product = null;
                            orderDetail = null;
                        }
                        response.statusCode = HttpStatusCode.Created;
                        orderHeader = null;
                    }
                    else
                    {
                        //UPDATE PROCESS
                        strStatus = "Updated";

                        existingHeader = (VMTblTOrder)GetById(data.Id ?? 0).data;

                        if (existingHeader != null)
                        {
                            orderHeader.Id = (int)existingHeader.Id;
                            orderHeader.TrxCode = existingHeader.TrxCode;
                            orderHeader.CreateBy = existingHeader.CreateBy;
                            orderHeader.CreateDate = existingHeader.CreateDate ?? DateTime.Now;

                            orderHeader.CustomerId = data.CustomerId;
                            orderHeader.Amount = data.Amount;
                            orderHeader.TotalQty = data.TotalQty;
                            orderHeader.IsCheckout = data.IsCheckout;

                            orderHeader.UpdatedBy = data.UpdatedBy;
                            orderHeader.UpdateDate = DateTime.Now;

                            //UPDATE ORDER HEADER
                            db.Update(orderHeader);
                            db.SaveChanges();

                            foreach (VMTblTOrderDetail item in data.orderDetail)
                            {
                                //must input id order detail !
                                existingDetail = existingHeader.orderDetail.FirstOrDefault(od => od.Id == item.Id);

                                orderDetail = new TblTOrderDetail();

                                item.Qty = (orderHeader.IsDeleted == true ? 0 : item.Qty);
                                orderDetail.Qty = item.Qty;
                                orderDetail.Price = item.Price;
                                orderDetail.ProductId = item.ProductId;
                                orderHeader.UpdatedBy = orderHeader.UpdatedBy;
                                orderHeader.UpdateDate = orderHeader.UpdateDate;

                                orderDetail.Id = (int)existingDetail.Id;
                                orderDetail.OrderHeaderId = existingDetail.OrderHeaderId;
                                orderDetail.IsDeleted = existingDetail.IsDeleted;
                                orderDetail.CreateBy = existingDetail.CreateBy;
                                orderDetail.CreateDate = existingDetail.CreateDate ?? DateTime.Now;

                                db.Update(orderDetail);
                                db.SaveChanges();

                                //UPDATE PRODUCT STOCK
                                product = db.TblMProducts.Find(item.ProductId);

                                if (product != null)
                                {
                                    if(existingDetail.Qty > item.Qty)
                                    {
                                        product.Stock += (existingDetail.Qty - item.Qty);
                                    }
                                    else
                                    {
                                        //CHECK IF PRODUCT STOCK IS ENOUGH
                                        if (product.Stock >= (item.Qty - existingDetail.Qty))
                                        {
                                            product.Stock -= (item.Qty - existingDetail.Qty);
                                            
                                        }
                                        else
                                        {
                                            response.statusCode = HttpStatusCode.BadRequest;
                                            throw new Exception($"Product {item.ProductName} with ID = {item.ProductId} does not have enough stock");
                                        }
                                    }
                                    //UPDATE PRODUCT STOCK DATA
                                    db.Update(product);
                                    db.SaveChanges();

                                }
                                else
                                {
                                    response.statusCode = HttpStatusCode.NotFound;
                                    throw new ArgumentNullException($"Product {item.ProductName} with ID = {item.ProductId} NOT FOUND");
                                }
                                product = null;
                                orderDetail = null;
                                existingDetail = null;
                            }
                            orderHeader = null;
                        }
                        else
                        {
                            response.statusCode = HttpStatusCode.NotFound;
                            throw new Exception($"Order with ID = {data.Id} doesn't exist");
                        }
                        
                        response.statusCode = HttpStatusCode.OK;
                    }
                    
                    dbTran.Commit();
                    response.data = data;
                    response.message = $"The new order data has been succesfully {strStatus}!";
                }
                catch (Exception e)
                {
                    dbTran.Rollback();
                    response.message = $"Order data can't {strStatus}! {e.Message}";
                }
            }
                return response;
        }

        private string TrxCodeGenerator(int? lastId)
        {
            string trxCodePrefix = $"XA-{DateTime.Now.ToString("ddMMyyyy")}-";
            string trxNo = (lastId != null || lastId > 0) 
                ? (lastId + 1).ToString().PadLeft(5,'0') : "00001";

            return string.Concat(trxCodePrefix, trxNo);
        }
    }
}
