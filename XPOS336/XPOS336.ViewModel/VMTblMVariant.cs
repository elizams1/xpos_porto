﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace XPOS336.ViewModel
{
    
    public class VMTblMVariant
    {
        
        public int? Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }
        public bool? IsDeleted { get; set; }


        public int CategoryId { get; set; }
        public string? CategoryName { get; set; }


        public int CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdatedBy { get; set; }        
        public DateTime? UpdateDate { get; set; }
    }
}
