﻿namespace XPOS336.ViewModel
{
    public class VMTblMCategory
    {

        public int? Id { get; set; }
        public string CategoryName { get; set; } = null!;
       
        public string? Description { get; set; }
        
        public bool? IsDeleted { get; set; }
        
        public int CreateBy { get; set; }
        
        public DateTime? CreateDate { get; set; }
        
        public int? UpdatedBy { get; set; }
        
        public DateTime? UpdateDate { get; set; }
    }
}
