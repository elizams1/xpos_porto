﻿namespace XPOS336.ViewModel
{
    public class VMTblMCustomer
    {
        public int? Id { get; set; }
        
        public string Name { get; set; } = null!;
        
        public string Email { get; set; } = null!;
        
        public string Password { get; set; } = null!;
       
        public string Address { get; set; } = null!;
        
        public string? Phone { get; set; }
        
        public int? RoleId { get; set; }
        
        public bool? IsDeleted { get; set; }
        
        public int CreateBy { get; set; }
        
        public DateTime? CreateDate { get; set; }
        
        public int? UpdatedBy { get; set; }
        
        public DateTime? UpdateDate { get; set; }
    }
}
