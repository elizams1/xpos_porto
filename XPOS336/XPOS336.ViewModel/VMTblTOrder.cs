﻿using System.Net;

namespace XPOS336.ViewModel
{
    public class VMTblTOrder
    {
        
        public int? Id { get; set; }
        public string? TrxCode { get; set; }
        public decimal Amount { get; set; }
        public int TotalQty { get; set; }
        public bool IsCheckout { get; set; }
        public bool? IsDeleted { get; set; }


        public int CustomerId { get; set; }
        public string? CustomerName { get; set; }


        public int CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdateDate { get; set; }

        public List<VMTblTOrderDetail>? orderDetail { get; set; }
    }
}
